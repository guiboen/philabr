---
title: "Calendário"
date: 2022-10-08T12:30:47-03:00
draft: false
---
Outubro

Pré Temporada:
- 03/10 76ers x Nets (V)
- 05/10 76ers x Cavaliers(V)
- 10/10 76ers x Cavaliers
- 12/10 76ers x Hornets

Temporada regular:
- 18/10 76ers x Celtics
- 20/10 76ers x Bucks
- 22/10 76ers x Spurs
- 24/10 76ers x Pacers
- 26/10 76ers x Raptors
- 28/10 76ers x Raptors
- 29/10 76ers x Bulls 
- 31/10 76ers x Wizards 